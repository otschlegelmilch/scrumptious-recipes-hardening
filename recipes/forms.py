from django import forms

# try:
from recipes.models import Recipe


# except Exception:
#     pass


from recipes.models import Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
