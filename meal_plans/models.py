from dataclasses import field
from re import template
from django.conf import settings
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from recipes.models import Recipe
# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL

class MealPlan(models.Model):
    name = models.CharField(max_length=125)
    date = models.DateField()
    owner = models.ForeignKey(USER_MODEL, related_name="meal_plans", on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="meal_plans",)
    
    def __str__(self):
        return self.name + " by " + str(self.owner)

    