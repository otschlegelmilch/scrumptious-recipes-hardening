from django.contrib import admin
from recipes.models import Recipe, Measure, FoodItem, Ingredient, Step, Rating
from meal_plans.models import MealPlan
# Register your models here.

class MealPlanAdmin(admin.ModelAdmin):
    pass

admin.site.register(MealPlan, MealPlanAdmin)
