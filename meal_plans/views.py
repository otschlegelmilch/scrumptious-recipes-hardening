from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plans.models import MealPlan

# Create your views here.
class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 2
    context_object_name = "meal_plans_list"


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"
    context_object_name = "mealplan"

    def deletePlan(request, id):
        item = item.objects.get(id=id)
        item.delete()
        return redirect('index')
    

class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "plans/new.html"
    fields = ["name"]
    success_url = reverse_lazy("meal_plans_list")
    
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
        
    def create_meal_plan(self, request):
        if request.method == "POST":
            form = MealPlan(request.POST)
            if form.is_valid():
                plan = form.save(commit=False)
                plan.owner = self.request.user
                plan.save()
                form.save_m2m()
            return redirect("meal_plans_list", pk=plan.id)

class MealPlanUserView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "plans/edit.html"
    fields = ["name", "author", "description"]
    success_url = reverse_lazy("meal_plans_list")


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

    def deletePlan(request, id):
        item = item.objects.get(id=id)
        item.delete()
        return redirect('index')

