from django.contrib.auth import views as auth_views
from django.urls import path

from meal_plans.views import MealPlanListView, MealPlanCreateView, MealPlanDetailView, MealPlanDeleteView

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path("new/", MealPlanCreateView.as_view(), name="meal_plans_new"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plan_detail"),
    path("<int:id>/", MealPlanDeleteView.as_view, name = "delete")
]
