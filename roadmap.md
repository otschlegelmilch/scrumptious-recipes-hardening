## ListView
- [ ] shows only user plans
- [ ] make link to create new meal plans
- [ ] each plan should be a link to detail view
  - [ ] each detail view of the meals should have links to recipe detail 

## DetailView
- [ ] make recipe show up
  - [ ] update template tag names, so data is populated
  - [ ] add a link to the edit view & delete view
  - [ ] make it so only the owner of the meal plan can see it
  
## Edit View
- [ ] make a form to allow the user to edit the plan (add button to edit, in the detail view)
  - [ ] make it so only the owner of the meal plan can edit it
## Delete View
- [ ] show form to the owner to delete the plan (add button to delete, in the detail view)
  - [ ] redirect owner back to list view after deletion

## Templates
- [ ] add "Login" link to the navigation list for the base.html
  - [ ] looks like
<li>
  <a href="{% url 'login' %}">Login</a>
</li> 

# Questions
- how to make it so only the owner can see/edit/delete a meal plan??
- how to make it so the user gets redirected after deleting a page (or anything else)?
- 